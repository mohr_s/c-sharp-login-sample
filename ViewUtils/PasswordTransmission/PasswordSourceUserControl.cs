﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ViewModelUtils;
using ViewModelUtils.PasswordTransmission;

namespace ViewUtils.PasswordTransmission
{
    public abstract class PasswordSourceUserControl : UserControl
    {
        private PasswordGetter _passwordGetter;
        private IPasswordSink _currentPasswordSink;

        protected PasswordSourceUserControl()
        {
            DataContextChanged += ManagePasswordGetter;
            Loaded += ManagePasswordGetter;
            Unloaded += ClearPasswordGetter;
            ManagePasswordGetter();
        }

        protected PasswordGetter PasswordGetter
        {
            private get => _passwordGetter;
            set
            {
                if (_passwordGetter != value)
                {
                    var oldGetter = _passwordGetter;
                    _passwordGetter = value;
                    CurrentPasswordSink?.ExchangePasswordSource(oldGetter, value);
                }
            }
        }

        private IPasswordSink CurrentPasswordSink
        {
            get => _currentPasswordSink;
            set
            {
                if (_currentPasswordSink != value)
                {
                    var oldSink = _currentPasswordSink;
                    _currentPasswordSink = value;
                    oldSink?.ClearPasswordSource(PasswordGetter);
                    _currentPasswordSink?.SetPasswordSource(PasswordGetter);
                }
            }
        }

        private void ClearPasswordGetter(object sender, RoutedEventArgs e)
            => ClearPasswordGetter();

        private void ClearPasswordGetter() => CurrentPasswordSink = null;

        private void ManagePasswordGetter(object sender, RoutedEventArgs e)
            => ManagePasswordGetter();

        private void ManagePasswordGetter(object sender, DependencyPropertyChangedEventArgs e)
            => ManagePasswordGetter();

        private void ManagePasswordGetter() => CurrentPasswordSink = DataContext as IPasswordSink;
    }
}
