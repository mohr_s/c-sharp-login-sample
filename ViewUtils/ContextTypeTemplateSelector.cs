﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ViewUtils
{

    /// <summary>
    /// A DataTemplateSelector that selects DataTemplates based on the <see cref="Type"/> of the
    /// <see cref="UserControl">UserControls</see> DataContext, or failing that the Type's BaseType (recursively).
    /// 
    /// A Default Template may be specified.
    /// </summary>
    public sealed class ContextTypeTemplateSelector : DataTemplateSelector
    {

        /// <summary>
        /// Dictionary to that contains the mapping of <see cref="Type"/> to <see cref="DataTemplate"/>.
        /// </summary>
        public Dictionary<Type, DataTemplate> TemplateMapping { get; } = new Dictionary<Type, DataTemplate>();

        /// <summary>
        /// A default <see cref="DataTemplate"/> in case none is found in the <see cref="TemplateMapping"/> dictionary.
        /// </summary>
        public DataTemplate DefaultTemplate { get; set; }
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var type = item?.GetType();
            while (type != null)
            {
                if (TemplateMapping.TryGetValue(type, out var ret))
                {
                    return ret;
                }
                else
                {
                    type = type.BaseType;
                }
            }
            return DefaultTemplate;
        }
    }
}
