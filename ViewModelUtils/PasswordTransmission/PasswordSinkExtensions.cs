﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelUtils.PasswordTransmission
{
    /// <summary>
    /// Extensions for the interface <see cref="IPasswordSink"/>
    /// </summary>
    public static class _
    {

        /// <summary>
        /// Method to swap out the <see cref="PasswordGetter"/> in a single line of code.
        /// </summary>
        /// <param name="passwordSink">The password sink whose password source should be replaced.</param>
        /// <param name="oldGetter">The password source to be replaced.</param>
        /// <param name="newGetter">The new password source.</param>
        /// <returns></returns>
        public static bool ExchangePasswordSource(this IPasswordSink passwordSink, PasswordGetter oldGetter, PasswordGetter newGetter)
            => passwordSink.ClearPasswordSource(oldGetter) ? passwordSink.SetPasswordSource(newGetter) : false;
    }
}
