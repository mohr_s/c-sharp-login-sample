﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelUtils.PasswordTransmission
{
    /// <summary>
    /// Delegate to transfer a password from a secret container to a <see cref="IPasswordSink"/>.
    /// </summary>
    /// <returns>The current password from the source container.</returns>
    public delegate string PasswordGetter();
}
