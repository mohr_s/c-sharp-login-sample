﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelUtils.PasswordTransmission
{
    /// <summary>
    /// Interface for ViewModels that desire a password source, used to work around WPF PasswordBoxes not being bindable.
    /// </summary>
    public interface IPasswordSink
    {
        /// <summary>
        /// Method to register a new <see cref="PasswordGetter"/>. May not overwrite other password sources.
        /// </summary>
        /// <param name="getter">The new password source.</param>
        /// <returns>False when a different PasswordSource has already been set, true otherwise.</returns>
        bool SetPasswordSource(PasswordGetter getter);

        /// <summary>
        /// Method to unregister a <see cref="PasswordGetter"/>.
        /// </summary>
        /// <param name="getter">The password source to be removed.</param>
        /// <returns>False if a different password getter is currently registered.
        /// True in case there was no getter or the getter has sucessfully been removed.</returns>
        bool ClearPasswordSource(PasswordGetter getter);
    }
}
