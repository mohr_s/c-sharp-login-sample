﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelUtils.ViewModels.Switcher
{
    /// <summary>
    /// A base class to that serves as a container for a ViewModel switcher. Contains a private method <see cref="SetState(TEnum)"/>
    /// for switching the state of the class and exchange the ViewModel <see cref="CurrentVM"/>.
    /// </summary>
    /// <typeparam name="TEnum">Enum that defines the states this container can adopt.</typeparam>
    /// <typeparam name="TItem">Base type of the items managed by this container. Must derive from <see cref="AbstractVMSwitchItem{T}"/></typeparam>
    public abstract class AbstractVMSwitchContainer<TEnum, TItem> : AbstractVMBase
    where TEnum : System.Enum
        where TItem : AbstractVMSwitchItem<TEnum>
    {
        private TItem _currentVM;

        /// <summary>
        /// Constructor to initialize the container. Requires the initial state.
        /// </summary>
        /// <param name="initial">Defines the initial state of the container and the initial value of the <see cref="CurrentVM"/></param>
        public AbstractVMSwitchContainer(TEnum initial) => SetState(initial);

        /// <summary>
        /// Property that propagetes the active ViewModel of this container.
        /// </summary>
        public TItem CurrentVM
        {
            get => _currentVM;
            private set => SetProperty(ref _currentVM, value);
        }

        /// <summary>
        /// Method to change the state of the container.
        /// This method is supposed to be passed on to all corresponding <see cref="AbstractVMSwitchItem{T}">AbstractVMSwitchItems</see>
        /// to serve as a callback once the <see cref="CurrentVM"/> is no longer desired.
        /// </summary>
        /// <param name="newState">The state towards the container is supposed to change.</param>
        protected void SetState(TEnum newState) => CurrentVM = GetStateViewModel(newState);

        /// <summary>
        /// Converts the enum state description of the container into the corresponding <see cref="AbstractVMSwitchItem{T}"/>.
        /// </summary>
        /// <param name="state">Enum value describing the new state of the container.</param>
        /// <returns>The <see cref="AbstractVMSwitchItem{T}"/> that belongs to the desired state.</returns>
        protected abstract TItem GetStateViewModel(TEnum state);

    }

    /// <summary>
    /// Derived class of <see cref="AbstractVMSwitchContainer{TEnum, TItem}"/> used when a specification of the CurrentVM property
    /// is not desired.
    /// </summary>
    /// <typeparam name="TEnum">Enum that defines the states this container can adopt.</typeparam>
    public abstract class AbstractVMSwitchContainer<TEnum> : AbstractVMSwitchContainer<TEnum, AbstractVMSwitchItem<TEnum>>
        where TEnum : System.Enum
    {

        /// <summary>
        /// Constructor to initialize the container. Requires the initial state.
        /// </summary>
        /// <param name="initial">Defines the initial state of the container and the initial value of the CurrentVM/></param>
        public AbstractVMSwitchContainer(TEnum initial) : base(initial) { }
    }
}
