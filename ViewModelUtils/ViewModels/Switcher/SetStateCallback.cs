﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelUtils.ViewModels.Switcher
{
    /// <summary>
    /// Delegate to communicate the desired state of a <see cref="AbstractVMSwitchContainer{TEnum, TItem}"/> from a
    /// <see cref="AbstractVMSwitchItem{T}"/>. Serves as a callback to trigger the state change in the container.
    /// </summary>
    /// <typeparam name="T">Enum type to contrain the possible states.</typeparam>
    /// <param name="newState">The desired state that the container should adopt.</param>
    public delegate void SetStateCallback<T>(T newState);
}
