﻿namespace ViewModelUtils.ViewModels.Switcher
{
    /// <summary>
    /// Base type for possible items of a <see cref="AbstractVMSwitchContainer{TEnum, TItem}"/>.
    /// 
    /// Contains a callback to inform the container of state changes.
    /// </summary>
    /// <typeparam name="T">Enum type to constrain the possible states.</typeparam>
    public class AbstractVMSwitchItem<T> : AbstractVMBase
        where T : System.Enum
    {
        /// <summary>
        /// The <see cref="SetStateCallback{T}"/> used to trigger state changes
        /// in the corresponding <see cref="AbstractVMSwitchContainer{TEnum, TItem}"/>.
        /// </summary>
        protected SetStateCallback<T> StateCallback { get; }

        /// <summary>
        /// Constructor that stores the callback in the property <see cref="StateCallback"/>
        /// </summary>
        /// <param name="stateCallback"></param>
        protected AbstractVMSwitchItem(SetStateCallback<T> stateCallback)
            => StateCallback = stateCallback;
    }
}