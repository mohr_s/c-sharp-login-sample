﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelUtils.ViewModels
{
    /// <summary>
    /// Base class for ViewModels, implements the <see cref="INotifyPropertyChanged"/> interface and provides a single line property setter. 
    /// </summary>
    public abstract class AbstractVMBase : INotifyPropertyChanged
    {
        /// <inheritdoc /> 
        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
        /// Helper method to handle setter of ViewModel properties and trigger the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <typeparam name="T">Type of the Property that is being changed.</typeparam>
        /// <param name="property">Reference to the field that is being overwritten.</param>
        /// <param name="newValue">The new value of the field.</param>
        /// <param name="caller">Optional property defined by <see cref="CallerMemberNameAttribute"/>.</param>
        protected void SetProperty<T>(ref T property, T newValue, [CallerMemberName] string caller = null)
        {
            if (caller == null) { 
                throw new InvalidOperationException("Method may not be called from Lambda Expression.");
            }

            if (!EqualityComparer<T>.Default.Equals(property, newValue))
            {
                property = newValue;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
            }
        }
    }
}
