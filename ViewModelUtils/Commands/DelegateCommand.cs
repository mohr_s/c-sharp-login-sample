﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ViewModelUtils.Commands
{
    /// <summary>
    /// Container object that serves offers an elswhere defined <see cref="Action{Object}"/> as an <see cref="ICommand"/>.
    /// 
    /// Commands can be (de)activated via a callback <see cref="CommandStateSetter"/>.
    /// </summary>
    public class DelegateCommand : ICommand
    {

        /// <summary>
        /// Wraps an <see cref="Action"/> into an <see cref="ICommand"/>. Disabled unless specified otherwise.
        /// </summary>
        /// 
        /// <param name="action">The action triggered by the <see cref="Execute(object)"/> method.</param>
        /// <param name="stateSetter">A callback delegate to change the (in)active state.</param>
        /// <param name="enabled">Initial state of the command.</param>
        public DelegateCommand(Action<object> action, out CommandStateSetter stateSetter, bool enabled = false)
        {
            Action = action;
            Enabled = enabled;
            stateSetter = StateSetter;
        }

        private void StateSetter(bool enabled)
        {
            if (Enabled != enabled)
            {
                Enabled = enabled;
                CanExecuteChanged?.Invoke(this, null);
            }
        }

        private Action<object> Action { get; }
        private bool Enabled { get; set; }

        /// <inheritdoc />
        public event EventHandler CanExecuteChanged;

        /// <inheritdoc />
        public bool CanExecute(object parameter) => Enabled;

        /// <inheritdoc />
        public void Execute(object parameter)
        {
            if (!CanExecute(parameter))
            {
                throw new InvalidOperationException("Cannot execute disabled command.");
            }
            Action(parameter);
        }
    }
}
