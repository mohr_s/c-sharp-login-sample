﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelUtils.Commands
{
    /// <summary>
    /// Delegate to alter the enabled/disabled state of a Command.
    /// </summary>
    /// <param name="enabled">true, if the command should be enabled.</param>
    public delegate void CommandStateSetter(bool enabled);
}
