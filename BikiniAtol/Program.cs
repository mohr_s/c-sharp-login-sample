﻿using System;
using System.Text.RegularExpressions;
using ViewUtils;

namespace BikiniAtol
{
    class Program
    {

        static void Main(string[] args)
        {

            var d = new ContextTypeTemplateSelector();

            Console.WriteLine(Regex.IsMatch("foo@bar.com", @"$[a-z]+@[a-z][a-z0-9]*(\.[a-z][a-z0-9]*)+^"));
            Console.WriteLine(Regex.IsMatch("2foo@bar.com", @"^[a-z]+@[a-z][a-z0-9]*(\.[a-z][a-z0-9]*)+$"));
            Console.WriteLine(Regex.IsMatch("2foo@bar.com", @"[a-z]+@[a-z][a-z0-9]*(\.[a-z][a-z0-9]*)+"));
            Console.ReadLine();
        }

    }
}
