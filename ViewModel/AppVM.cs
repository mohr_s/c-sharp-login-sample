﻿using System;
using ViewModelUtils;
using ViewModelUtils.ViewModels.Switcher;

namespace ViewModel
{
    public sealed class AppVM : AbstractVMSwitchContainer<AppState>
    {
        private AppVMCommonData CommonData { get; } = new AppVMCommonData();

        public AppVM() : base(AppState.Login) { }

        protected override AbstractVMSwitchItem<AppState> GetStateViewModel(AppState state)
        {
            switch (state)
            {
                case AppState.Login:
                    return new LoginVM(CommonData, SetState);
                case AppState.Session:
                    return new SessionVM(CommonData, SetState);
                default:
                    throw new ArgumentException($"State {state} is unknown.", nameof(state));
            }
        }
    }
}
