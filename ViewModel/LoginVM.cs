﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModelUtils;
using ViewModelUtils.Commands;
using ViewModelUtils.PasswordTransmission;
using ViewModelUtils.ViewModels.Switcher;

namespace ViewModel
{
    public sealed class LoginVM : AbstractAppVMItem, IPasswordSink
    {

        private string _userHandle = string.Empty;

        public string UserHandle
        {
            get => _userHandle;
            set => SetProperty(ref _userHandle, value);
        }
        public ICommand LoginCommand { get; }
        private CommandStateSetter LoginPossibleSetter { get; }

        private PasswordGetter PasswordSource { get; set; }

        internal LoginVM(AppVMCommonData commonData, SetStateCallback<AppState> callback) : base(commonData, callback)
        {
            LoginCommand = new DelegateCommand(PerformLogin, out var stateSetter, false);
            LoginPossibleSetter = stateSetter;
            PropertyChanged += UpdateLoginActive;
            UpdateLoginActive(null, null);
        }

        private void UpdateLoginActive(object sender, PropertyChangedEventArgs args)
        {
            if (sender == this && args.PropertyName == nameof(UserHandle))
            {
                LoginPossibleSetter(
                    Regex.IsMatch(UserHandle, @"^[a-z_][a-z0-9_-]*(\.[a-z0-9_-]+)*@[a-z][a-z0-9_-]*(\.[a-z][a-z0-9_-]*)+$"));
            }
        }

        private void PerformLogin(object _)
        {
            CommonData.Session = new Session(UserHandle, PasswordSource?.Invoke());
            StateCallback(AppState.Session);
        }

        public bool ClearPasswordSource(PasswordGetter getter)
        {
            bool ret = PasswordSource == getter;
            if (ret)
            {
                PasswordSource = null;
            }
            return ret;
        }

        public bool SetPasswordSource(PasswordGetter getter)
        {
            if (PasswordSource == null)
            {
                PasswordSource = getter;
                return true;
            }
            else
            {
                return PasswordSource == getter;
            }
        }
    }
}
