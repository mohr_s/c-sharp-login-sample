﻿
using ViewModelUtils;
using ViewModelUtils.ViewModels.Switcher;

namespace ViewModel
{
    public abstract class AbstractAppVMItem : AbstractVMSwitchItem<AppState>
    {
        private protected AppVMCommonData CommonData { get; }
        internal AbstractAppVMItem(AppVMCommonData commonData, SetStateCallback<AppState> callback)
            : base(callback)
            => CommonData = commonData;
    }
}
