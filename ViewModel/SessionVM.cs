﻿using System;
using ViewModelUtils;
using ViewModelUtils.ViewModels.Switcher;

namespace ViewModel
{
    public class SessionVM : AbstractAppVMItem
    {
        internal SessionVM(AppVMCommonData commonData, SetStateCallback<AppState> callback) : base(commonData, callback) { }

        public string Name { get; set; }
    }
}