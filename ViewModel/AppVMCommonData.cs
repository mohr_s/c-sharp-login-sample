﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    /// <summary>
    /// Container that contains the Data shared between the Sub-ViewModels
    /// </summary>
    internal sealed class AppVMCommonData
    {
        public Session Session { get; set; }
    }
}
