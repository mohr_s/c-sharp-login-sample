﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ViewModelUtils;
using ViewModelUtilTests.Stubs;

namespace ViewModelUtilTests
{
    [TestClass]
    public class AbstractVMSwitchContainerTests
    {

        public int CallCount { get; private set; }
        public VMState CallState { get; private set; }
        public VMSwitchItemStub CallReturn { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            CallCount = 0;
            CallState = default;
        }

        public VMSwitchContainerStub Create(VMState initState)
        {
            GetStateViewModelProxy proxy = (state, callback) =>
            {
                CallCount++;
                CallState = state;
                CallReturn = new VMSwitchItemStub(callback);
                Assert.AreEqual(null, VMSwitchContainerStub.PullableProxy);
                return CallReturn;
            };

            VMSwitchContainerStub.PullableProxy = proxy;
            var ret = new VMSwitchContainerStub(initState);
            return ret;
        }
        
        [TestMethod]
        public void InitialStateTest()
        {

            var targetState = VMState.C;

            var container = Create(targetState);

            Assert.AreEqual(targetState, CallState);
            Assert.AreEqual(1, CallCount);
            Assert.AreEqual(CallReturn, container.CurrentVM);

        }

        [TestMethod]
        public void StateChangeTest()
        {

            var targetState = VMState.C;

            var container = Create(default);
            var oldCallCount = CallCount;
            var oldReturn = CallReturn;
            container.StateSetter(targetState);

            Assert.AreEqual(targetState, CallState);
            Assert.AreEqual(1 + oldCallCount, CallCount);
            Assert.AreEqual(CallReturn, container.CurrentVM);
            Assert.AreNotEqual(oldReturn, container.CurrentVM);

        }

        [TestMethod] 
        public void GenericInsertionConstructorTest()
        {
            foreach (var state in new VMState[] { VMState.A, VMState.B, VMState.C })
            {
                var container = new VMSwitchContainerGenericInsertionStub(state);
                Assert.AreEqual(1, container.StateViewModelCallCount);
                Assert.AreEqual(state, container.State);
            }
        }
    }
}
