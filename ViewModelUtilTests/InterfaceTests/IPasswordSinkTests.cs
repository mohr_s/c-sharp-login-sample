﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModelUtils;
using ViewModelUtils.PasswordTransmission;

namespace ViewModelUtilTests.InterfaceTests
{
    public abstract class IPasswordSinkTests
    {
        protected PasswordGetter SourceA { get; } = () => "Foo";
        protected PasswordGetter SourceB { get; } = () => "Bar";

        public IPasswordSink TestObject { get; set; }
        
        public abstract void Init();

        [TestMethod]
        public void SuccessfulSet()
        {
            Assert.IsTrue(TestObject.SetPasswordSource(SourceA));
            Assert.IsTrue(TestObject.SetPasswordSource(SourceA));
        }
        [TestMethod]
        public void SuccessfulClear()
        {
            Assert.IsTrue(TestObject.SetPasswordSource(SourceA));
            Assert.IsTrue(TestObject.ClearPasswordSource(SourceA));
        }

        [TestMethod]
        public void ClearEmpty()
        {
            Assert.IsTrue(TestObject.ClearPasswordSource(SourceA));
            Assert.IsTrue(TestObject.ClearPasswordSource(SourceB));
        }
        [TestMethod]
        public void FailedClear()
        {
            Assert.IsTrue(TestObject.SetPasswordSource(SourceA));
            Assert.IsFalse(TestObject.ClearPasswordSource(SourceB));
        }
        [TestMethod]
        public void FailedSet()
        {
            Assert.IsTrue(TestObject.SetPasswordSource(SourceA));
            Assert.IsFalse(TestObject.SetPasswordSource(SourceB));
        }
    }
}
