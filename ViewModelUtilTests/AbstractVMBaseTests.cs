﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel;
using ViewModelUtilTests.Stubs;

namespace ViewModelUtilTests
{
    public abstract class AbstractVMBaseTests<T>
    {
        protected T Value { get; }
        protected AbstractVMBaseTests(T value) => Value = value;

        protected VMBaseStub<T> TestObject { get; set; }
        public int CallCount { get; private set; }
        public VMBaseStub<T> CallSource { get; private set; }
        public string CallProperty { get; private set; }


        private void CounterIncrement(object source, PropertyChangedEventArgs property) =>
                CallCount++;

        [TestInitialize]
        public void Init()
        {
            TestObject = new VMBaseStub<T>();
            CallCount = 0;
            CallSource = (VMBaseStub<T>)null;
            CallProperty = (string)null;
        }

        [TestMethod]
        public void TestCounterIncrement()
        {
            Assert.AreEqual(0, CallCount);
            CounterIncrement(null, null);
            Assert.AreEqual(1, CallCount);
        }

        [TestMethod]
        public void NoCallerExceptionTest()
        {
            try { 
                TestObject.NullCall();
                Assert.Fail();
            } catch (Exception e)
            {
                Assert.AreEqual(typeof(InvalidOperationException), e.GetType());
            }
        }
        [TestMethod]
        public void TestGetSet()
        {
            TestObject.Property = Value;
            Assert.AreEqual(Value, TestObject.Property);
        }

        [TestMethod]
        public void PropertyChangedNotificationTest()
        {

            TestObject.PropertyChanged += (source, property) =>
            {
                CallCount++;
                CallSource = source as VMBaseStub<T>;
                CallProperty = property.PropertyName;
            };

            TestObject.Property = Value;
            Assert.AreEqual(1, CallCount);
            Assert.AreEqual(TestObject, CallSource);
            Assert.AreEqual(nameof(VMBaseStub<T>.Property), CallProperty);
        }

        [TestMethod]
        public void PropertyUnchangedDefaultNotificationTest()
        {

            TestObject.PropertyChanged += CounterIncrement;

            TestObject.Property = default;
            Assert.AreEqual(0, CallCount);
        }

        [TestMethod]
        public void PropertyUnchangedSetTwiceNotificationTest()
        {

            TestObject.Property = Value;

            TestObject.PropertyChanged += CounterIncrement;

            TestObject.Property = Value;

            Assert.AreEqual(0, CallCount);
        }

        [TestMethod]
        public void PropertyChangedNotificationUnsubscribedTest()
        {
            PropertyChangedEventHandler lambda = CounterIncrement;
            TestObject.PropertyChanged += lambda;
            TestObject.PropertyChanged -= lambda;

            TestObject.Property = Value;
            Assert.AreEqual(0, CallCount);
        }
    }

    [TestClass]
    public sealed class ObjectPropertyTests : AbstractVMBaseTests<object>
    {
        public ObjectPropertyTests() : base(new object())
        {
        }
    }

    [TestClass]
    public sealed class StringPropertyTests : AbstractVMBaseTests<string>
    {
        public StringPropertyTests() : base("Test")
        {
        }

        public int StringCallCount { get; private set; }

        [TestInitialize]
        public void InitString() => StringCallCount = 0;

        [TestMethod]
        public void PropertyUnchangedSetValueEqualNotificationTest()
        {

            TestObject.Property = Value;

            TestObject.PropertyChanged += (source, property) =>
            {
                StringCallCount++;
            };

            TestObject.Property = new string(Value.ToCharArray());

            Assert.AreEqual(0, StringCallCount);
            TestObject.Property = null;
            Assert.AreEqual(1, StringCallCount);
        }

            
}

}
