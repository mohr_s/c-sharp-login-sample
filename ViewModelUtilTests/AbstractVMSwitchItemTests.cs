﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModelUtils;
using ViewModelUtilTests.Stubs;

namespace ViewModelUtilTests
{
    [TestClass]
    public class AbstractVMSwitchItemTests
    {
        public int CallCount { get; private set; }

        [TestInitialize]
        public void Init() => CallCount = 0;
        [TestMethod]
        public void TestCallback()
        {
            void callback(VMState state) => CallCount++;
            var item = new VMSwitchItemStub(callback);
            Assert.AreEqual(callback, item.StateSetter);
            Assert.AreEqual(0, CallCount);
            item.StateSetter(VMState.A);
            Assert.AreEqual(1, CallCount);
        }
    }
}
