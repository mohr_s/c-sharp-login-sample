﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModelUtilTests.InterfaceTests;
using ViewModelUtilTests.Stubs;

namespace ViewModelUtilTests
{
    [TestClass]
    public class IPasswordSinkTestsTests : IPasswordSinkTests
    {
        [TestInitialize]
        public override void Init() => TestObject = new PasswordSinkStub();

        [TestMethod]
        public void ConfirmPasswordSources()
        {
            Assert.IsFalse(SourceA == SourceB);
            Assert.IsNotNull(SourceA);
            Assert.IsNotNull(SourceB);
            Assert.IsFalse(SourceA() == SourceB());
            Assert.IsNotNull(SourceA());
            Assert.IsNotNull(SourceB());
        }
    }
}