﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModelUtils;
using ViewModelUtils.PasswordTransmission;

namespace ViewModelUtilTests.Stubs
{
    public class PasswordSinkStub : IPasswordSink
    {
        private PasswordGetter PasswordSource { get; set; }

        public bool ClearPasswordSource(PasswordGetter getter)
        {
            if (PasswordSource == getter)
            {
                PasswordSource = null;
            }
            return PasswordSource == null;
        }
        public bool SetPasswordSource(PasswordGetter getter)
        {
            if (PasswordSource == null)
            {
                PasswordSource = getter;
            }
            return PasswordSource == getter;
        }
    }
}
