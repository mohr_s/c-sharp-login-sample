﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModelUtils;
using ViewModelUtils.ViewModels;

namespace ViewModelUtilTests.Stubs
{
    public class VMBaseStub<T>: AbstractVMBase
    {

        private T _property;

        public T Property
        {
            get => _property;
            set => SetProperty(ref _property, value);
        }

        public void NullCall()
        {
            int i = 0;
            SetProperty(ref i, 0, null);
        }
    }
}
