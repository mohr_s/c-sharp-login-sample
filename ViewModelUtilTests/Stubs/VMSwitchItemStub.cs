﻿using ViewModelUtils;
using ViewModelUtils.ViewModels.Switcher;

namespace ViewModelUtilTests.Stubs
{
    public class VMSwitchItemStub : AbstractVMSwitchItem<VMState>
    {
        public VMSwitchItemStub(SetStateCallback<VMState> callback) : base(callback) { }
        public SetStateCallback<VMState> StateSetter => StateCallback;
    }
}
