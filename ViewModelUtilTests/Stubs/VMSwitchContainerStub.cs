﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModelUtils;
using ViewModelUtils.ViewModels.Switcher;

namespace ViewModelUtilTests.Stubs
{
    public enum VMState { A = 1, B, C };

    public delegate VMSwitchItemStub GetStateViewModelProxy(VMState state, SetStateCallback<VMState> callback);

    public class VMSwitchContainerStub : AbstractVMSwitchContainer<VMState, VMSwitchItemStub>
    {
        public VMSwitchContainerStub(VMState initial) : base(initial)
        {
        }

        private GetStateViewModelProxy Proxy { get; set; }

        public static GetStateViewModelProxy PullableProxy { get; set; }

        public SetStateCallback<VMState> StateSetter => SetState;

        protected override VMSwitchItemStub GetStateViewModel(VMState state)
        {
            if (Proxy == null)
            {
                Proxy = PullableProxy;
                PullableProxy = null;
            }
            return Proxy(state, SetState);
        }
    }

    public class VMSwitchContainerGenericInsertionStub : AbstractVMSwitchContainer<VMState>
    {
        public VMSwitchContainerGenericInsertionStub(VMState state) : base(state) { }

        public VMState State { get; private set; }
        public int StateViewModelCallCount { get; private set; } = 0;
        protected override AbstractVMSwitchItem<VMState> GetStateViewModel(VMState state)
        {
            State = state;
            StateViewModelCallCount++;
            return null;
        }
    }
}
