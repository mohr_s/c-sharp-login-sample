﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModelUtils;
using ViewModelUtils.PasswordTransmission;

namespace ViewModelUtilTests
{
    [TestClass]
    public class IPasswordSinkExtensionsTest
    {
        IPasswordSink TestObject { get; set; }

        protected PasswordGetter SourceA { get; } = () => "Foo";
        protected PasswordGetter SourceB { get; } = () => "Bar";

        [TestInitialize]
        public void Init() => TestObject = A.Fake<IPasswordSink>();


        [TestMethod]
        public void ConfirmPasswordSources()
        {
            Assert.IsFalse(SourceA == SourceB);
            Assert.IsNotNull(SourceA);
            Assert.IsNotNull(SourceB);
            Assert.IsFalse(SourceA() == SourceB());
            Assert.IsNotNull(SourceA());
            Assert.IsNotNull(SourceB());
        }

        [TestMethod]
        public void SuccessfulExchange()
        {
            A.CallTo(() => TestObject.ClearPasswordSource(SourceA)).Returns(true);
            A.CallTo(() => TestObject.SetPasswordSource(SourceB)).Returns(true);

            Assert.IsTrue(TestObject.ExchangePasswordSource(SourceA, SourceB));
            A.CallTo(() => TestObject.ClearPasswordSource(SourceA)).MustHaveHappenedOnceExactly();
            A.CallTo(() => TestObject.SetPasswordSource(SourceB)).MustHaveHappenedOnceExactly();

        }

        [TestMethod]
        public void FailedExchange()
        {
            A.CallTo(() => TestObject.ClearPasswordSource(SourceA)).Returns(false);
            A.CallTo(() => TestObject.SetPasswordSource(SourceB)).Returns(true);

            Assert.IsFalse(TestObject.ExchangePasswordSource(SourceA, SourceB));
            A.CallTo(() => TestObject.ClearPasswordSource(SourceA)).MustHaveHappenedOnceExactly();
            A.CallTo(() => TestObject.SetPasswordSource(SourceB)).MustNotHaveHappened();

        }
    }
}
