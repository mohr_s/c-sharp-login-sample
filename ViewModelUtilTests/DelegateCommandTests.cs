﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModelUtils;
using ViewModelUtils.Commands;

namespace ViewModelUtilTests
{
    [TestClass]
    public class DelegateCommandTests
    {
        private CommandStateSetter _stateSetter;
        private ref CommandStateSetter StateSetter => ref _stateSetter;

        public DelegateCommand TestObject { get; private set; }
        public object LastObject { get; private set; }
        public int ExecutionCount { get; private set; }

        [TestInitialize]
        public void Init()
        {
            ExecutionCount = 0;
            EventSource = null;
            EventRaisedCount = 0;
            LastObject = null;
            StateSetter = null;
            TestObject = new DelegateCommand((o) => { ExecutionCount++; LastObject = o; }, out StateSetter);
        }

        [TestMethod]
        public void DefaultStateTest()
        {
            Assert.IsFalse(TestObject.CanExecute(null));
            StateSetter(false);
            Assert.IsFalse(TestObject.CanExecute(null));

        }

        [TestMethod]
        public void DefaultStateExecutionException()
        {
            try
            {
                TestObject.Execute(null);
                Assert.Fail("Exception should have been thrown.");
            } catch (Exception e)
            {
                Assert.AreEqual(typeof(InvalidOperationException), e.GetType());
                Assert.AreEqual(0, ExecutionCount);
            }
        }

        [TestMethod]
        public void OnOffStateExecutionExcutionException()
        {
            StateSetter(true);
            Assert.IsTrue(TestObject.CanExecute(null));
            StateSetter(false);
            Assert.IsFalse(TestObject.CanExecute(null));
            DefaultStateExecutionException();
        }
        [TestMethod]
        public void SuccessfulExecution()
        {
            StateSetter(true);
            var input = new object();
            TestObject.Execute(input);
            Assert.AreEqual(1, ExecutionCount);
            Assert.AreEqual(input, LastObject);
        }

        public int EventRaisedCount { get; set; }
        public object EventSource { get; set; }
        [TestMethod]
        public void EnabledStateNotificationTest()
        {
            TestObject.CanExecuteChanged += (source, args) =>
            {
                EventRaisedCount++;
                EventSource = source;
            };

            StateSetter(false);
            try
            {
                TestObject.Execute(new object());
            } catch
            {
                Assert.AreEqual(null, EventSource);
                Assert.AreEqual(0, EventRaisedCount);
            }

            StateSetter(true);
            Assert.AreEqual(TestObject, EventSource);
            Assert.AreEqual(1, EventRaisedCount);

            StateSetter(true);
            Assert.AreEqual(1, EventRaisedCount);

        }
    }
}
